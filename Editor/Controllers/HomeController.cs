﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Web.Mvc;
using Editor.Models;
using EnvDTE;
using Newtonsoft.Json.Linq;

namespace Editor.Controllers
{
    public class HomeController : Controller
    {
        private static string slnName = ((DTE)Marshal.GetActiveObject("VisualStudio.DTE.14.0")).Solution.FullName;
        private static string slnDir = Path.GetDirectoryName(slnName);
        private static string prjDir = AppDomain.CurrentDomain.BaseDirectory;

        private const string tilesetPath = "\\Content\\Tilesets";
        private const string mapsPath = "\\Assets\\Maps";

        public ActionResult Index()
        {
            var model = new EditorViewModel();

            DirectoryInfo tilesetDir = new DirectoryInfo(string.Format("{0}{1}", prjDir, tilesetPath));
            FileInfo[] tilesets = tilesetDir.GetFiles("*.png");

            foreach (FileInfo tileset in tilesets)
            {
                model.Tilesets.Add(tileset.Name);
            }

            DirectoryInfo mapDir = new DirectoryInfo(string.Format("{0}{1}", slnDir, mapsPath));
            FileInfo[] maps = mapDir.GetFiles("*.json");

            foreach (FileInfo map in maps)
            {
                model.AvailableMaps.Add(map.Name.Substring(0, map.Name.IndexOf(".json")));
            }

            return View(model);
        }

        public JsonResult Load(string m)
        {
            var file = string.Format("{0}{1}\\{2}.json", slnDir, mapsPath, m);
            var mapData = System.IO.File.ReadAllText(file).ToString();

            return Json(mapData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Save(MapDataModel model)
        {
            if (ModelState.IsValid)
            {
                var json = JObject.Parse(model.MapData);
                var mapName = json["Name"].ToString();
                var file = string.Format("{0}{1}\\{2}.json", slnDir, mapsPath, mapName);
                System.IO.File.WriteAllText(file, model.MapData);

                return Json(new { success = true, responseText = string.Format("Successfully saved map {0}", mapName) }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = false, responseText = "Error saving map" }, JsonRequestBehavior.AllowGet);
        }
    }
}