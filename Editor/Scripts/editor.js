﻿
// http://jsfiddle.net/SZuwh/   

var Editor = function () {
    // DOM Elements
    var gridCanvas = document.getElementById('grid');
    var mapCanvas = document.getElementById('map');
    var selectedCanvas = document.getElementById('selected');
    var tileCanvas = document.getElementById('tileset');

    var mapSelector = document.getElementById('mapSelector');
    var tilesetSelector = document.getElementById('tilesetSelector');
    var layerSelectors = document.getElementsByClassName('layer-selectors')[0];
    var mapConnections = document.getElementsByClassName('map-Connection');
    var allLayers = document.getElementById('allLayers');

    var solidCb = document.getElementById('solidCb');
    var layer0 = document.getElementById('layer0');

    var addLayerBtn = document.getElementById('addLayerBtn');
    var minusLayerBtn = document.getElementById('minusLayerBtn');

    var requestedLayers = document.getElementById('requestedLayers');
    var mapName = document.getElementById('mapName');

    var saveForm = document.getElementById('saveForm');
    var leftPanel = document.getElementById('canvas-container');
    var radioButtons = document.getElementsByName('solidType');
    var npc = document.getElementById('npcRb');
    var chest = document.getElementById('chestRb');

    // Renderers
    var gridRenderer = new Renderer(gridCanvas, 960, 800);
    var mapRenderer = new Renderer(mapCanvas, 960, 800);
    var selectedRenderer = new Renderer(selectedCanvas, 32, 32);
    var tileRenderer = new Renderer(tileCanvas, 552, 610);

    // Misc
    var mapLoaded = false;
    var sourceTileX = -1;
    var sourceTileY = -1;
    var tilesetImage = new Image();

    // Collections
    var mapRenderers = [mapRenderer];
    var mapCanvases = [mapCanvas];
    var mapsLoaded = [mapLoaded];
    var layerSelectorCol = [layer0];

    var createElements = function (checked, canvasDisplay, idx, callBack) {
        let layerSelector = document.createElement('div')
        layerSelector.setAttribute('class', 'layer-selector');

        let checkbox = document.createElement('input');
        checkbox.setAttribute('type', 'checkbox');
        checkbox.setAttribute('name', 'layer' + idx);
        checkbox.setAttribute('data-index', idx);
        checkbox.setAttribute('class', 'layerCb');
        if (checked)
            checkbox.setAttribute('checked', 'checked');

        checkbox.addEventListener('change', layerSelectorEvt);

        let label = document.createElement('label');
        label.setAttribute('for', checkbox.name);
        label.innerHTML = 'Layer ' + idx;

        layerSelector.appendChild(checkbox);
        layerSelector.appendChild(label);
        layerSelectors.appendChild(layerSelector);

        let newCanvas = document.createElement('canvas');
        newCanvas.setAttribute('id', 'map' + idx);
        newCanvas.style.zIndex = idx;
        newCanvas.style.display = canvasDisplay;

        mapCanvases.push(newCanvas);

        if (!mapRenderers[idx])
            mapRenderers.push(new Renderer(newCanvas, 960, 800));

        leftPanel.appendChild(newCanvas);

        callBack(newCanvas, idx);
    };

    var loadMapConnections = function (_mapCon, result) {
        for (let i = 0; i < _mapCon.length; i++) {
            let dir = _mapCon[i].getAttribute('data-direction');
            if (result[dir] != null)
                _mapCon[i].value = result[dir];
            else
                _mapCon[i].selectedIndex = 0;

            allMapData[dir] = result[dir];
        }
    }

    var loadMap = function (response) {
        let result = JSON.parse(response);
        allMapData.TileData = result.TileData;
        tilesetSelector.value = result.Tileset;
        tilesetImage.src = '../Content/Tilesets/' + result.Tileset;
        mapName.value = result.Name;
        allMapData.Layers = requestedLayers.value = result.Layers;
        loadMapConnections(mapConnections, result);

        while (mapCanvas.nextElementSibling) {
            mapCanvas.parentNode.removeChild(mapCanvas.nextElementSibling);
        }

        mapRenderer.context.clearRect(0, 0, mapCanvas.width, mapCanvas.height);

        while (layer0.parentNode.nextElementSibling) {
            layer0.parentNode.parentNode.removeChild(layer0.parentNode.nextElementSibling);
        }

        mapsLoaded = [];
        mapCanvases = [mapCanvas];
        mapRenderers = [mapRenderer];

        mapRenderer.renderMap(allMapData.TileData[0].Coords, tilesetImage);
        mapsLoaded[0] = true;
        allLayers.checked = true;
        if (allMapData.Layers > 1) {
            for (let i = 1; i < allMapData.Layers; i++) {
                createElements(true, 'block', i, function (_canvas, idx) {
                    removeMapCanvasEvents(_canvas, idx);
                    registerMapCanvasEvents(_canvas, idx);
                    mapRenderers[idx].renderMap(allMapData.TileData[idx].Coords, tilesetImage);
                    mapsLoaded[idx] = true;
                });
            }
        }
    };

    // Events
    var tileMouseMoveEvt = function (e) {
        let x = (((e.pageX - 90) / tileRenderer.tilesize) | 0) - 32;
        let y = (((e.pageY - 45) / tileRenderer.tilesize) | 0);
        let tileX = x * tileRenderer.tilesize;
        let tileY = y * tileRenderer.tilesize;

        if (tileRenderer.lastX == x && tileRenderer.lastY == y) return;

        let point = tileRenderer.context.getImageData(tileX, tileY, 32, 32).data;
        let isWhite = true;

        for (let i = 0; i < point.length; i++) {
            isWhite = isWhite && point[i] == 0;
        }

        if (!isWhite) {
            tileRenderer.renderTileSet(tilesetImage, tileRenderer.lastX, tileRenderer.lastY);
            tileRenderer.drawHoverRect('red', tileX, tileY);
        }

        tileRenderer.lastX = x;
        tileRenderer.lastY = y;
    };

    var tileClickEvt = function (e) {
        let x = (((e.pageX - 90) / tileRenderer.tilesize) | 0) - 32;
        let y = ((e.pageY - 45) / tileRenderer.tilesize) | 0;

        sourceTileX = x * tileRenderer.tilesize;
        sourceTileY = y * tileRenderer.tilesize;

        selectedRenderer.drawTile(tilesetImage, 0, 0, sourceTileX, sourceTileY);
    };

    var mapMouseMoveEvt = function (e) {
        let mapRender = mapRenderers[e.target.indexParam];
        let x = ((e.pageX - 15) / mapRenderer.tilesize) | 0;
        let y = ((e.pageY - 15) / mapRenderer.tilesize) | 0;
        let tileX = x * mapRenderer.tilesize;
        let tileY = y * mapRenderer.tilesize;

        if (mapRenderer.lastX == x && mapRenderer.lastY == y) return;

        mapRenderer.lastX = x;
        mapRenderer.lastY = y;
    };

    var mapClickEvt = function (e) {
        let mapRenderer = mapRenderers[e.target.indexParam];
        let x = ((e.pageX - 15) / mapRenderer.tilesize) | 0;
        let y = ((e.pageY - 15) / mapRenderer.tilesize) | 0;

        let destinationX = x * mapRenderer.tilesize;
        let destinationY = y * mapRenderer.tilesize;

        if (sourceTileX > -1 && sourceTileY > -1) {
            mapRenderer.drawTile(tilesetImage, destinationX, destinationY, sourceTileX, sourceTileY);

            let tile = allMapData.TileData[e.target.indexParam].Coords[y][x];
            tile.Coordinates[0] = sourceTileX;
            tile.Coordinates[1] = sourceTileY;
            tile.Solid = solidCb.checked;

            if (solidCb.checked) {
                if (npc.checked)
                    tile.Type = "npc";
                if (chest.checked)
                    tile.Type = "chest";
            }
            else
                tile.Type = "tile";
        }
    };

    var layerSelectorEvt = function (e) {
        let index = e.target.getAttribute('data-index');
        mapCanvases[index].style.display = this.checked ? 'block' : 'none';
        if (this.checked) {
            if (allMapData.TileData[index]) {
                mapRenderers[index].renderMap(allMapData.TileData[index].Coords, tilesetImage);
            }
            else
                mapRenderers[index].renderMap(null, tilesetImage);
        }
    };


    var tilesetSelectorChangeEvt = function (e) {
        if (e.target.selectedIndex > 0) {
            tilesetImage.src = '../Content/Tilesets/' + e.target.value;
            tileRenderer.context.clearRect(0, 0, 640, 640);
            tileRenderer.context.drawImage(tilesetImage, 0, 0);
        }
    };

    var solidCbClickEvt = function (e) {
        for (let i = 0; i < radioButtons.length; i++) {
            radioButtons[i].disabled = !this.checked;
            if (!this.checked)
                radioButtons[i].checked = false;
        }
    };

    var addLayerMouseUpEvt = function (e) {
        let currentLayerValue = parseInt(requestedLayers.value);
        requestedLayers.value = currentLayerValue + 1;

        createElements(false, 'none', currentLayerValue, function (_canvas) {
            allMapData.TileData.push(new DefaultCoords());
            allMapData.Layers += 1;
            registerMapCanvasEvents(_canvas, currentLayerValue);
        });
    };

    var minusLayerMouseUpEvt = function (e) {
        if (leftPanel.childElementCount > 2 && layerSelectors.childNodes.length > 2) {
            requestedLayers.value = parseInt(requestedLayers.value) - 1;

            let lastSelector = layerSelectors.lastElementChild;
            layerSelectors.removeChild(lastSelector);

            let lastCanvas = leftPanel.lastElementChild;
            leftPanel.removeChild(lastCanvas);
            mapRenderers.splice(mapRenderers.length - 1, 1);

            mapCanvases[parseInt(requestedLayers.value)].removeEventListener('mousemove', mapMouseMoveEvt);
            mapCanvases[parseInt(requestedLayers.value)].removeEventListener('click', mapClickEvt);
            mapCanvases.splice(parseInt(requestedLayers.value), 1);

            allMapData.TileData.splice(allMapData.TileData.length - 1, 1);
            allMapData.Layers -= 1;
        }
    };

    var saveFormSubmitEvt = function (e) {
        e.preventDefault();
        e.stopPropagation();

        allMapData.Name = mapName.value;
        allMapData.Tileset = tilesetSelector.options[tilesetSelector.selectedIndex].value;

        ajax('POST', '/Home/Save', JSON.stringify({ mapData: JSON.stringify(allMapData) }), function (response) {
            var result = JSON.parse(response);
            alert(result.responseText);
        }, function (response) {
            var result = JSON.parse(response);
            alert(result.responseText);
        });
    };

    var mapSelectorChangeEvt = function (e) {
        if (e.target.selectedIndex > 0) {          

            ajax('POST', '/Home/Load', JSON.stringify({ m: mapSelector.value }), function (response) {
                loadMap(JSON.parse(response));
            });
        }
    };

    var allLayersChangeEvt = function (e) {
        let layerCbs = document.getElementsByClassName('layerCb');
        for (let i = 0; i < layerCbs.length; i++)
        {
            layerCbs[i].checked = this.checked;
            layerCbs[i].dispatchEvent(new Event('change'));
        }
    }

    // Event Registration Functions
    var registerTileCanvasEvents = function () {
        tileCanvas.addEventListener('mousemove', tileMouseMoveEvt);
        tileCanvas.addEventListener('click', tileClickEvt);
    };

    var registerMapCanvasEvents = function (_canvas, idx) {
        _canvas.indexParam = idx;

        _canvas.addEventListener('mousemove', mapMouseMoveEvt);
        _canvas.addEventListener('click', mapClickEvt);
    };

    var removeMapCanvasEvents = function (_canvas, idx) {
        _canvas.indexParam = idx;

        _canvas.removeEventListener('mousemove', mapMouseMoveEvt);
        _canvas.removeEventListener('click', mapClickEvt);
    };

    var mapConnectionChangeEvt = function (e) {
        let direction = e.target.getAttribute('data-direction');
        if (this.selectedIndex > 0)
            allMapData[direction] = this.value;
        else {
            allMapData[direction] = null;
        }
    }

    var registerEvents = function () {
        for (let i = 0; i < mapCanvases.length; i++) {
            registerMapCanvasEvents(mapCanvases[i], i);
        }

        registerTileCanvasEvents();

        tilesetSelector.addEventListener('change', tilesetSelectorChangeEvt);
        solidCb.addEventListener('click', solidCbClickEvt);
        layer0.addEventListener('change', layerSelectorEvt);

        addLayerBtn.addEventListener('mouseup', addLayerMouseUpEvt);
        minusLayerBtn.addEventListener('mouseup', minusLayerMouseUpEvt);

        saveForm.addEventListener('submit', saveFormSubmitEvt);

        mapSelector.addEventListener('change', mapSelectorChangeEvt);

        for (let i = 0; i < mapConnections.length; i++) {
            mapConnections[i].addEventListener('change', mapConnectionChangeEvt);
        }

        allLayers.addEventListener('change', allLayersChangeEvt);

    };

    this.Init = function () {
        registerEvents();

        tilesetImage.onload = function () {
            tileRenderer.context.drawImage(tilesetImage, 0, 0);
        }

        gridRenderer.renderGridLines();
    }
}

var init = function () {
    let editor = new Editor();
    editor.Init();
}