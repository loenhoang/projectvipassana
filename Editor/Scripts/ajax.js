﻿var ajax = function (method, url, data, callBack, errorCallBack) {
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {
                callBack(xmlhttp.responseText);
            }
            else if (xmlhttp.status == 400) {
                if (errorCallBack)
                    errorCallBack(xmlhttp.responseText);
                else
                    alert('Error code: ' + xmlhttp.status);
            }
            else {
                alert('Error code: ' + xmlhttp.status);
            }
        }
    };

    xmlhttp.open(method, url, true);
    xmlhttp.setRequestHeader('Content-Type', 'application/json');
    xmlhttp.send(data);
};