﻿var Renderer = function (canvas, width, height) {
    this.canvas = canvas;
    this.context = canvas.getContext('2d');
    this.tilesize = 32;

    this.resize(width, height);

    this.lastX = 0;
    this.lastY = 0;
}

Renderer.prototype.resize = function (width, height) {
    this.canvas.width = width;
    this.canvas.height = height;
}

Renderer.prototype.getWidth = function () {
    return this.canvas.width;
}

Renderer.prototype.getHeight = function () {
    return this.canvas.height;
}

Renderer.prototype.drawCellRect = function (color, x, y) {
    this.clearRect(x, y);
    this.context.save();
    this.context.lineWidth = 2;
    this.context.strokeStyle = color;
    this.context.translate(x, y);
    this.context.strokeRect(0, 0, this.tilesize, this.tilesize);
    this.context.restore();
}

Renderer.prototype.drawHoverRect = function (color, x, y) {
    this.context.save();
    this.context.lineWidth = 1;
    this.context.strokeStyle = color;
    this.context.translate(x, y);
    this.context.strokeRect(1, 1, this.tilesize - 2, this.tilesize - 2);
    this.context.restore();
}

Renderer.prototype.drawTile = function (image, x, y, sx, sy) {
    this.context.save();
    this.context.drawImage(image, sx, sy, this.tilesize, this.tilesize, x, y, this.tilesize, this.tilesize);
    this.context.restore();
}

Renderer.prototype.clearRect = function (x, y) {
    this.context.clearRect(x, y, this.tilesize, this.tilesize);
}

Renderer.prototype.renderTileAt = function (map, tilesheet, i, j) {
    var row = i * this.tilesize;
    var col = j * this.tilesize;

    if (map && map[0] > -1 && map[1] > -1) {
        this.drawTile(tilesheet, col, row, map[0], map[1]);
    } else {
    }
}

Renderer.prototype.renderGridLines = function () {
    for (var i = 0; i < (this.getHeight() / this.tilesize) ; i++)
        for (var j = 0; j < (this.getWidth() / this.tilesize) ; j++) {
            var row = i * this.tilesize;
            var col = j * this.tilesize;
            this.drawCellRect('#333', col, row);
        }
}

Renderer.prototype.renderMap = function (map, tilesheet) {
    for (var i = 0; i < (this.getHeight() / this.tilesize) ; i++)
        for (var j = 0; j < (this.getWidth() / this.tilesize) ; j++)
            this.renderTileAt(map[i][j].Coordinates, tilesheet, i, j);
}

Renderer.prototype.renderTileSet = function (tileset, x, y) {
    this.clearRect(x * this.tilesize, y * this.tilesize);
    this.context.save();
    this.context.drawImage(tileset, 0, 0);
    this.context.restore();
}