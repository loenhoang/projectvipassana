﻿using System.ComponentModel.DataAnnotations;

namespace Editor.Models
{
    public class MapDataModel
    {
        [Required]
        public string MapData { get; set; }
    }
}