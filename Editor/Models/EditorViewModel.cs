﻿using System.Collections.Generic;

namespace Editor.Models
{
    public class EditorViewModel
    {
        private List<string> tilesets;
        public List<string> Tilesets
        {
            get
            {
                if (tilesets == null)
                    tilesets = new List<string>();
                return tilesets;
            }
            set
            {
                tilesets = value;
            }
        }
        private List<string> availableMaps;

        public List<string> AvailableMaps
        {
            get
            {
                if (availableMaps == null)
                    availableMaps = new List<string>();
                return availableMaps;
            }
            set
            {
                availableMaps = value;
            }
        }
    }
}