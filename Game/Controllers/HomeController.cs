﻿using System.IO;
using System.Runtime.InteropServices;
using System.Web.Mvc;
using EnvDTE;

namespace Game.Controllers
{
    public class HomeController : Controller
    {
        private static string slnName = ((DTE)Marshal.GetActiveObject("VisualStudio.DTE.14.0")).Solution.FullName;
        private static string slnDir = Path.GetDirectoryName(slnName);

        private const string mapsPath = "\\Assets\\Maps";

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public JsonResult Load(string m)
        {
            var file = string.Format("{0}{1}\\{2}.json", slnDir, mapsPath, m);
            var mapData = System.IO.File.ReadAllText(file).ToString();

            return Json(mapData, JsonRequestBehavior.AllowGet);
        }
    }
}