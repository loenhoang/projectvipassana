﻿var MapLoader = new (function () {
    let tilesheetPath = '../Content/Tilesets/';
    let tilesize = 32;
    let tileImage = new Image();
    let height = 800;
    let width = 960;

    let renderMap = function (map, tilesheet) {
        for (var i = 0; i < (height / tilesize) ; i++)
            for (var j = 0; j < (width / tilesize) ; j++) {
                let row = i * tilesize;
                let col = j * tilesize;
                let cell = map[i][j];

                if (cell.Coordinates && cell.Coordinates[0] > -1 && cell.Coordinates[1] > -1) {
                    mapCtx.save();
                    mapCtx.drawImage(tilesheet, cell.Coordinates[0], cell.Coordinates[1], tilesize, tilesize, col, row, tilesize, tilesize);
                    mapCtx.restore();
                }
            }
    }

    var SetSolids = function (map) {
        for (let i = 0; i < (height / tilesize) ; i++) {            
            for (let j = 0; j < (width / tilesize) ; j++) {
                solids[i][j] = map[i][j].Solid;
            }
            solids.push([]);
        }

        return solids;
    }

    this.LoadMap = function (map) {
        return ajax.Post('/Home/Load', JSON.stringify({ m: map })).then(function (response) {
            var mapData = JSON.parse(JSON.parse(response));
            tileImage.src = tilesheetPath + mapData.Tileset;
            tileImage.onload = function () {
                for (let i = 0; i < mapData.Layers; i++) {
                    renderMap(mapData.TileData[i].Coords, tileImage);
                }
            }

            return SetSolids(mapData.TileData[0].Coords);
            
        }, function (error) {
            console.log("error!", error);
        });
    }
    var solids = [[]];
    this.Solids = function () {
        return solids;
    }

    this.TileSize = function () {
        return tilesize;
    }
});