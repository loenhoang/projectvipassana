﻿var Character = function (imgPath, frameWidth, frameHeight, startX, startY) {
    var sprite = new SpriteSheet(imgPath, frameWidth, frameHeight);

    var animations = {
        walkDown: new Animation(sprite, 1, 0, 7),
        walkUp: new Animation(sprite, 1, 8, 15),
        walkLeft: new Animation(sprite, 1, 16, 23),
        walkRight: new Animation(sprite, 1, 24, 31),
        idle: new Animation(sprite, 1, 0, 0),
        idleDown: new Animation(sprite, 1, 0, 0),
        idleUp: new Animation(sprite, 1, 8, 8),
        idleLeft: new Animation(sprite, 1, 16, 16),
        idleRight: new Animation(sprite, 1, 24, 31)
    }

    var posX = startX;
    var posY = startY;
    var lastPosX;
    var lastPosY;
    var distanceTravelled = 0;

    function changePosition(animation, canMove) {

        if (animation == 'walkLeft') {
            posX -= canMove ? 4 : -4;
            distanceTravelled += canMove ? 4 : 0;
        }
        else if (animation == 'walkRight') {
            posX += canMove ? 4 : -4;
            distanceTravelled += canMove ? 4 : 0;
        }
        else if (animation == 'walkDown') {
            posY += canMove ? 4 : -4;
            distanceTravelled += canMove ? 4 : 0;
        }
        else if (animation == 'walkUp') {
            posY -= canMove ? 4 : -4;
            distanceTravelled += canMove ? 4 : 0;
        }
    }

    this.SnapToGrid = function (animation) {

        if (distanceTravelled >= 32) {
            distanceTravelled = 0;            
        }

        animations[animation].Clear(posX, posY);
        posX = Math.ceil(posX / 32) * 32;
        posY = Math.ceil(posY / 32) * 32;
        animations[animation].Render(posX, posY);
    }

    this.Animate = function (animation, canMove) {
        if (animation in animations) {
            animations[animation].Clear(posX, posY);
            changePosition(animation, canMove);
            animations[animation].Render(posX, posY);
        }
    }

    this.PosX = function () {
        return posX;
    }
    this.PosY = function () {
        return posY;
    }
    this.Width = function () {
        return sprite.frameWidth;
    }
    this.Height = function () {
        return sprite.frameHeight;
    }

}