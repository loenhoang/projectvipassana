﻿var Input= new (function () {
    let KEY_LEFT = 37;
    let KEY_RIGHT = 39;
    let KEY_UP = 38;
    let KEY_DOWN = 40;
    let KEY_ESC = 27;

    let keysDown = {
        left: false,
        down: false,
        right: false,
        up: false,
        esc: false
    }

    let keysUp = {
        left: false,
        down: false,
        right: false,
        up: false,
        esc: false
    }

    let isKeyDown = false;

    let SetKeysDown = function (e) {
        ResetKeysDown();

        if (e.keyCode == KEY_LEFT) {
            keysDown.left = isKeyDown = true;
        }
        else if (e.keyCode == KEY_DOWN) {
            keysDown.down = isKeyDown = true;
        }
        else if (e.keyCode == KEY_RIGHT) {
            keysDown.right = isKeyDown = true;

        }
        else if (e.keyCode == KEY_UP) {
            keysDown.up = isKeyDown = true;
        }
        else if (e.keyCode == KEY_ESC) {
            keysDown.esc = !keysDown.esc;
        }
    }

    let SetKeysUp = function (e) {
        ResetKeysUp();
        if (e.keyCode == KEY_LEFT) {
            keysUp.left = true;
        }
        else if (e.keyCode == KEY_DOWN) {
            keysUp.down = true;
        }
        else if (e.keyCode == KEY_RIGHT) {
            keysUp.right = true;
        }
        else if (e.keyCode == KEY_UP) {
            keysUp.up = true;
        }
        else if (e.keyCode == KEY_ESC) {
            keysUp.esc = !keysUp.esc;
        }
    }

    let Init = (function () {
        document.addEventListener('keydown', SetKeysDown);
        document.addEventListener('keyup', SetKeysUp);
    })();

    this.KeyDown = function (_key) {
        if (_key in keysDown)
            return keysDown[_key];
    }

    this.KeyUp = function (_key) {
        if (_key in keysUp)
            return keysUp[_key];
    }

    this.PlayerMoving = function () {
        return isKeyDown;
    }

    function ResetKeysDown() {
        keysDown.left = false;
        keysDown.down = false;
        keysDown.right = false;
        keysDown.up = false;
        isKeyDown = false;
    }

    function ResetKeysUp() {
        keysUp.left = false;
        keysUp.down = false;
        keysUp.right = false;
        keysUp.up = false;
        isKeyDown = false;
    }
});