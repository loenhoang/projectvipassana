﻿var ajax = new (function(){
    this.Post = function (url, data) {
        return new Promise(function (resolve, reject) {
            var req = new XMLHttpRequest();
            req.onload = function () {
                if (req.status == 200) {
                    resolve(req.response);
                }
                else {
                    reject(Error(req.statusText));
                }
            };

            req.onerror = function () {
                reject(Error("Network error"));
            }

            req.open('POST', url);
            req.setRequestHeader('Content-Type', 'application/json');
            req.send(data);
        });
        
    }
});