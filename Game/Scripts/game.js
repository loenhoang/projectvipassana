﻿var FPS = 30;
var canvas = document.getElementById('game');
var mapCanvas = document.getElementById('map');
var menuCanvas = document.getElementById('menu');

canvas.height = 800;
canvas.width = 960;
mapCanvas.height = 800;
mapCanvas.width = 960;
menuCanvas.height = 800;
menuCanvas.width = 960;

var ctx = canvas.getContext('2d');
var mapCtx = mapCanvas.getContext('2d');
var menuCtx = menuCanvas.getContext('2d');

var player = new Character('rpg_sprite_walk.png', 24, 32, 32, 32);

var solidTiles = [[]];
var mapLoaded = false;
var paused = false;

MapLoader.LoadMap('map123').then(function (response) {
    solidTiles = response; mapLoaded = true;
});

(function gameLoop() {
    if (paused == false) {
        menuCanvas.setAttribute('style', 'display:none');
        setTimeout(function () {
            requestAnimationFrame(gameLoop);
            if (mapLoaded) {
                if (Input.PlayerMoving()) {
                    if (Input.KeyDown('left')) {
                        var coordX = ((Math.ceil(player.PosX()) / 32) | 0);
                        var coordY = ((Math.ceil(player.PosY()) / 32) | 0);
                        if (player.PosX() >= canvas.clientLeft && coordX > 0) {
                            var nextTileSolid = !solidTiles[coordY][coordX];
                            player.Animate('walkLeft', nextTileSolid);
                        }
                        else
                            player.Animate('walkLeft', false);

                    }
                    else if (Input.KeyDown('down')) {
                        var coordX = ((Math.ceil(player.PosX()) / 32) | 0);
                        var coordY = ((Math.ceil(player.PosY() / 32)) + 1 | 0);
                        if (player.PosY() + player.Height() <= canvas.clientTop + canvas.clientHeight) {
                            var nextTile = !solidTiles[coordY][coordX];
                            player.Animate('walkDown', nextTile);
                        }
                        else
                            player.Animate('walkDown', false);

                    }
                    else if (Input.KeyDown('right')) {
                        var coordX = ((Math.ceil(player.PosX()) / 32) + 1 | 0);
                        var coordY = ((Math.ceil(player.PosY()) / 32) | 0);
                        if (player.PosX() + 32 <= canvas.clientLeft + canvas.clientWidth) {
                            var nextTileSolid = !solidTiles[coordY][coordX];
                            player.Animate('walkRight', nextTileSolid);
                        }
                        else
                            player.Animate('walkRight', false);
                    }
                    else if (Input.KeyDown('up')) {
                        var coordX = ((Math.ceil(player.PosX()) / 32) | 0);
                        var coordY = ((Math.ceil(player.PosY()) / 32) | 0);
                        if (player.PosY() >= canvas.clientTop && coordY > 0 && player.PosY() <= canvas.clientTop + canvas.clientHeight) {
                            var nextTile = !solidTiles[coordY][coordX];
                            player.Animate('walkUp', nextTile);
                        }
                        else
                            player.Animate('walkUp', false);
                    }
                }
                else {
                    paused = Input.KeyUp('esc');

                    var coordX = ((Math.ceil(player.PosX()) / 32) | 0);
                    var coordY = ((Math.ceil(player.PosY()) / 32) | 0);

                    if (Input.KeyUp('left')) {
                        if (player.PosX() >= canvas.clientLeft && coordX > 0) {
                            player.SnapToGrid('walkLeft');
                        }
                    }
                    else if (Input.KeyUp('down')) {
                        if (player.PosY() + player.Height() <= canvas.clientTop + canvas.clientHeight) {
                            player.SnapToGrid('walkDown');
                        }
                    }
                    else if (Input.KeyUp('right')) {
                        if (player.PosX() + 32 <= canvas.clientLeft + canvas.clientWidth) {
                            player.SnapToGrid('walkRight');
                        }
                    }
                    else if (Input.KeyUp('up')) {
                        if (player.PosY() >= canvas.clientTop && coordY > 0 && player.PosY() <= canvas.clientTop + canvas.clientHeight) {
                            player.SnapToGrid('walkUp');
                        }
                    }
                    player.Animate('idle');
                }
            }
            else {
                // Replace with loading gif or something
                player.Animate('idle');
            }
        }, 1000 / FPS);
    } else {
        menuCanvas.setAttribute('style', 'display:block');
        paused = Input.KeyUp('esc');
        requestAnimationFrame(gameLoop);
    }
})();

